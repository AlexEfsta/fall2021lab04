// Mohammad Khan, 2038700
import java.lang.Math.*;

public class Cone
{
    private double radius;
    private double height;

    public Cone(double radius, double height)
    {
        if(radius < 0)
        {
            this.radius = 1;
        }
        else
        {
            this.radius = radius;
        }
        if(height < 0)
        {
            this.radius = 1;
        }
        else
        {
            this.height = height;
        }
    }

    public double getRadius()
    {
        return this.radius;
    }

    public double getHeight()
    {
        return this.height;
    }

    public double getVolume()
    {
        return Math.PI*Math.pow(this.radius, 2)*(this.height/3);
    }
    public double getSurfaceArea()
    {
        return Math.PI*this.radius*(this.radius + Math.sqrt((Math.pow(this.height, 2) + Math.pow(this.radius, 2))));
    }
}