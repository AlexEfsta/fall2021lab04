//Alexander Efstathakis
import java.lang.Math.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class ConeTest {
    @Test public void coneVolume(){
        Cone coneTest = new Cone(2,4);
        Cone coneTest2 = new Cone(-1, -1);
        double vol2 = Math.PI*Math.pow(coneTest2.getRadius(), 2)*(coneTest2.getHeight()/3);
        double vol = Math.PI*Math.pow(coneTest.getRadius(), 2)*(coneTest.getHeight()/3);
        assertEquals(vol, coneTest.getVolume());
        assertEquals(vol2, coneTest2.getVolume());
    }

    @Test public void coneSurfaceArea(){
        Cone coneTest = new Cone(3,2);
        Cone coneTest2 = new Cone(-1, -1);
        double surfaceArea2 = Math.PI*coneTest2.getRadius()*(coneTest2.getRadius() + Math.sqrt((Math.pow(coneTest2.getHeight(), 2) + Math.pow(coneTest2.getRadius(), 2))));
        double surfaceArea = Math.PI*coneTest.getRadius()*(coneTest.getRadius() + Math.sqrt((Math.pow(coneTest.getHeight(), 2) + Math.pow(coneTest.getRadius(), 2))));
        assertEquals(surfaceArea, coneTest.getSurfaceArea());
        assertEquals(surfaceArea2, coneTest2.getSurfaceArea());
    }
}
