//Alexander Efstathakis
import java.lang.Math.*;
public class Sphere {
    private double radius;

    public Sphere(double r){
        if(r < 0){
            r = 1;
            System.out.println("a radius of a sphere can't be a negative, changing radius to 1");
        }
        this.radius = r;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getVolume(){
        return (4/3 * Math.PI * Math.pow(this.radius, 3));
    }

    public double getSurfaceArea(){
        return (4 * Math.PI * Math.pow(this.radius, 2));
    }
}
