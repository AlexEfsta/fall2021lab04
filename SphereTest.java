// Mohammad Khan, 2038700
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import java.lang.Math.*;

public class SphereTest
{
    @Test
    public void radiusTest()
    {
        Sphere sphere = new Sphere(5);
        assertEquals(5, sphere.getRadius());

        Sphere sphere2 = new Sphere(-1);
        assertEquals(1, sphere2.getRadius());
    }

    @Test
    public void volumeTest()
    {
        Sphere sphere = new Sphere(5);
        double vol = (4/3)*(Math.PI* Math.pow(sphere.getRadius(), 3));
        assertEquals(vol, sphere.getVolume());

        Sphere sphere2 = new Sphere(-1);
        double vol2 = (4/3)*(Math.PI* Math.pow(sphere2.getRadius(), 2));
        assertEquals(vol2, sphere2.getVolume());
    }

    @Test
    public void areaTest()
    {
        Sphere sphere = new Sphere(5);
        double area = 4*Math.PI*Math.pow(sphere.getRadius(), 2);
        assertEquals(area, sphere.getSurfaceArea());

        Sphere sphere2 = new Sphere(-1);
        double area2 = 4*Math.PI*Math.pow(sphere2.getRadius(), 2);
        assertEquals(area2, sphere2.getSurfaceArea());
    }
}